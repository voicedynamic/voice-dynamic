package com.thunderrise.smartsearch.base

import android.os.Parcel

/**
 * The basic data type in the project which includes
 * @id the word ID;
 * @typeWord (preposition, union, action);
 * @name the current word.
 */
data class Word(var id: Int, var typeWord: Int, var name: String) {

    constructor() : this(id = 0, typeWord = 0, name = "")

    constructor(name: String) : this(id = 0, typeWord = 0, name = name)

    constructor(word: Word) : this(word.id, word.typeWord, word.name)

    constructor(parcel: Parcel) : this(parcel.readInt(), parcel.readInt(), parcel.readString())

    override fun equals(other: Any?): Boolean {
        var flag = false
        if (other is Word) {
            if (name == other.name) {
                flag = true
            }
        }
        return flag
    }

    override fun toString(): String {
        return "$id $typeWord $name"
    }
}
