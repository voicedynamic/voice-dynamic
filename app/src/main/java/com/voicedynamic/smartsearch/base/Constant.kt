package com.voicedynamic.smartsearch.base

 object Constant {
    val YEAR = 44
    val MONTH = 45
    val DAY = 46
    val DAY_OF_WEEK = 47
    val PREPOSITION_DATE = 48
    val PREPOSITION_TIME = 49

    val SECOND_WORD = 52
    val MINUTE_WORD = 51
    val HOUR_WORD = 50
    val DAY_WORD = 43
    val WEEK_WORD = 68
    val YEAR_WORD = 41
    val MONTH_WORD = 42

    val HOUR = 53
    val MINUTE = 54
    val SECOND = 55
    val TIME_FULL = 1231

    //Date parser
    val FUTURE_DAY_MARKER = 58
    val PAST_DAY_MARKER = 59

    val FUTURE_YEAR_MARKER = 60
    val PAST_YEAR_MARKER = 61

    val FUTURE_MONTH_MARKER_WITH_NAME = 62
    val FUTURE_MONTH_MARKER = 63

    val PAST_MONTH_MARKER_WITH_NAME = 64
    val PAST_MONTH_MARKER = 65

    val FUTURE_WEEK_MARKER = 66
    val PAST_WEEK_MARKER = 67

    val FUTURE_DAY_MARKER_WITH_COUNT = 70


    val FIRST_DAY = 0
    val LAST_DAY = 31
    val DAY_IN_WEEK = 7

    val TYPE_NONE = -1
    val MONTH_NONE = -1
    val DAY_NONE = -1
    val WEEK_NONE = -1

    val YESTERDAY = "вчера"
    val TOMORROW = "завтра"

    val MORNING_MARKER = 156
    val EVENING_MARKER = 157
    val DINNER_MARKER = 159
    val MORNING = "утр"
    val EVENING = "вечер"
    val DINNER = "обед"

    val DATE_PREPOSITION : Int = 23
    val DATE_PREPOSITION_FUTURE = 34
    val DATE_PREPOSITION_PAST = 50

    val TIME_PREPOSITION_FUTURE = 81
    val TIME_PREPOSITION_PAST = 84

    val FUTURE_HOUR_MARKER = 85
    val PAST_HOUR_MARKER = 86

    val FUTURE_MINUTE_MARKER = 87
    val PAST_MINUTE_MARKER = 84

    val FUTURE_SECOND_MARKER = 88
    val PAST_SECOND_MARKER = 84
 }