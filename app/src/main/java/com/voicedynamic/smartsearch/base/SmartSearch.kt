package com.thunderrise.smartsearch

/**
 * The interface for smart searching. This interface includes a method
 * of searching for text that includes such fields:
 * @words text;
 * @status the status of this request (new or old);
 * @result Asynchronous response;
 */
interface SmartSearch {

    fun searchText(words: String, status: Status, result: CallBackResult)

    interface CallBackResult {

        fun onSuccess()

        fun onFailur()
    }

    enum class Status {
        NEW,
        ADDITION
    }
}