package com.thunderrise.smartsearch

import android.content.Context
import com.thunderrise.smartsearch.base.Word
import com.voicedynamic.smartsearch.parser.AppParserSentence
import com.voicedynamic.smartsearch.parser.ParserSentence

/**
 * Smart Search Nearest Neighbors.
 * The implementation of the search engine using the patern bilder.
 * @baseUrl The input will accept the basic URL address
 * @context for the interactions with the services
 */
class SmartSearchNearestNeighbors(baseUrl: String?, context: Context?) : SmartSearch {

    private var parserSentence: ParserSentence? = null

    private constructor(builder: Builder) : this(builder.baseUrl, builder.context) {
        parserSentence = AppParserSentence.newInstance()
    }

    class Builder {
        var baseUrl: String? = null
            private set

        var context: Context? = null
            private set

        fun setUrl(baseUrl: String) = apply { this.baseUrl = baseUrl }

        fun setContext(context: Context) = apply { this.context = context }

        fun build() = SmartSearchNearestNeighbors(this)
    }

    override fun searchText(
            sentence: String,
            status: SmartSearch.Status,
            result: SmartSearch.CallBackResult
    ) {
        var words: List<Word>
        if (parserSentence != null) {
            words = parserSentence!!.getWords(sentence)
        }
        result.onSuccess()
    }
}