package com.voicedynamic.smartsearch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.thunderrise.smartsearch.SmartSearch
import com.thunderrise.smartsearch.SmartSearchNearestNeighbors

class App : AppCompatActivity(), SmartSearch.CallBackResult {
    override fun onSuccess() {
    }

    override fun onFailur() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        var search: SmartSearch = SmartSearchNearestNeighbors.Builder()
                .setUrl("")
                .setContext(baseContext)
                .build()

        search.searchText("закажи автомобиль на 18:30", SmartSearch.Status.NEW, this)
    }
}
