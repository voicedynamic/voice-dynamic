package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word

/**
 * The sentence parser is the interface basic parser for
 * breaking a sentence into a list of words, thereby removing the
 * signs of the pripyinaya translating words into lower case
 */
interface ParserSentence {
    fun getWords(stringForParsing: String): List<Word>
}