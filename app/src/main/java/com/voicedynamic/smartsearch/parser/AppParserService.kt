package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word
import com.voicedynamic.smartsearch.model.Key
import com.voicedynamic.smartsearch.model.RelatedKey
import com.voicedynamic.smartsearch.model.RelatedKeyWeight

/**
 * The service parser is the primary parser for searching and selecting
 * the basic services in the system, as well as defining the first helper elements
 */
class AppParserService private constructor() : ParserServices {

    override val words: List<Word>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val attitudeDownOfCurrentWord: List<Key>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    /**
     * Search for additional service mass using refinements
     */
    override fun addWeightToServiceById(id: Int): List<RelatedKey> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Finding word relations in a sentence to search for a service
     */
    override fun findRelatedKeyWords(words: List<Word>): List<RelatedKeyWeight> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Getting names from the list of keys
     */
    override fun getNamesInKeys(keys: List<Key>): List<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Clear the parser of the data
     */
    override fun clear() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        private var instance: AppParserService? = null

        fun newInstance(): AppParserService? {
            if (instance == null) {
                instance = AppParserService()
            }
            return instance
        }
    }

}