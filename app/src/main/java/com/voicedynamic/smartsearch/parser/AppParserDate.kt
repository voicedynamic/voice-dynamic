package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word
import com.voicedynamic.smartsearch.base.Constant
import com.voicedynamic.smartsearch.model.DateTime
import java.util.*

/**
 * The date parser is a universal class for working with a
 * date in a textual representation based on the models given
 * get the data type date
 */
class AppParserDate(private var months: List<DateTime>,
                    private var dayOfWeeks: List<DateTime>,
                    private var datePrepositions: List<DateTime>,
                    private var timeDateKeyWords: List<DateTime>) : ParserDate {


    override fun getCalendarFromWords(words: List<Word>): Calendar? {
        val calendar = GregorianCalendar.getInstance()
        val currentCalendar = GregorianCalendar.getInstance()
        var year = calendar.get(Calendar.YEAR)
        var month = calendar.get(Calendar.MONTH)
        var day = calendar.get(Calendar.DAY_OF_MONTH)
        var isFutureDayMarker = false
        var futureDayMarkerCount = 0
        var weekChanges = false
        var type: Int

        if (checkTypeInSentence(words, Constant.FUTURE_DAY_MARKER_WITH_COUNT)) {
            isFutureDayMarker = true
        }

        for (word in words) {
            type = word.typeWord
            if (type == Constant.DAY) {
                if (isFutureDayMarker) {
                    futureDayMarkerCount = Integer.parseInt(word.name)
                } else {
                    val d = Integer.parseInt(word.name)
                    day = if (d > Constant.FIRST_DAY && d < Constant.LAST_DAY) d else currentCalendar.get(Calendar.DAY_OF_MONTH)
                }
            } else if (type == Constant.MONTH) {
                val m = getMonthNumber(word)
                month = if (m != Constant.MONTH_NONE) m else currentCalendar.get(Calendar.MONTH)
            } else if (type == Constant.YEAR) {
                val y = Integer.parseInt(word.name)
                year = if (y < 100) 1900 + y else y
            } else if (type == Constant.DAY_OF_WEEK) {
                val sentenceDay = getWeekDay(word)
                val currentDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
                if (sentenceDay != Constant.DAY_NONE) {
                    val difference = sentenceDay - currentDay
                    day += difference
                    if (difference < 0) {
                        weekChanges = true
                        day += Constant.DAY_IN_WEEK
                    }
                }
            }
        }

        for (word in words) {
            type = word.typeWord
            if (type == Constant.FUTURE_DAY_MARKER) {
                day++
            } else if (type == Constant.FUTURE_MONTH_MARKER_WITH_NAME) {
                if (month < Calendar.getInstance().get(Calendar.MONTH)) year++
            } else if (type == Constant.FUTURE_MONTH_MARKER) {
                month++
            } else if (type == Constant.FUTURE_WEEK_MARKER) {
                if (!weekChanges) day += 7
            } else if (type == Constant.FUTURE_YEAR_MARKER) {
                year ++
            } else if (type == Constant.PAST_DAY_MARKER) {
                day--
            } else if (type == Constant.PAST_MONTH_MARKER_WITH_NAME) {
                if (month > Calendar.getInstance().get(Calendar.MONTH)) year--
            } else if (type == Constant.PAST_MONTH_MARKER) {
                month--
            } else if (type == Constant.PAST_WEEK_MARKER) {
                day -= 7
            } else if (type == Constant.PAST_YEAR_MARKER) {
                year--
            }
        }

        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)

        if (isFutureDayMarker) {
            calendar.add(Calendar.DATE, futureDayMarkerCount)
        }

        val curCalendar = GregorianCalendar.getInstance()

        return if (calendar.get(Calendar.YEAR) == curCalendar.get(Calendar.YEAR) &&
                calendar.get(Calendar.MONTH) == curCalendar.get(Calendar.MONTH) &&
                calendar.get(Calendar.DAY_OF_MONTH) == curCalendar.get(Calendar.DAY_OF_MONTH)) {
            null
        } else calendar
    }

    override fun typeDateInWords(words: List<Word>): List<Word> {
        var words = setSimpleDateTypes(words)
        words = setComplexDateTypesOutOfOrdinary(words)
        return words
    }

    private fun checkTypeInSentence(words: List<Word>, type: Int): Boolean {
        return words.any { it.typeWord == type }
    }

    private fun setSimpleDateTypes(words: List<Word>): List<Word> {
        for (word in words) {
            word.typeWord = when {
                checkWordOnType(word, Constant.DAY_WORD) -> Constant.DAY_WORD
                checkWordOnType(word, Constant.MONTH_WORD) -> Constant.MONTH_WORD
                checkWordOnType(word, Constant.YEAR_WORD) -> Constant.YEAR_WORD
                checkWordOnType(word, Constant.WEEK_WORD) -> Constant.WEEK_WORD
                isWordBelongToDayOfWeek(word) -> Constant.DAY_OF_WEEK
                isWordBelongToMonth(word) -> Constant.MONTH
                else -> Constant.TYPE_NONE
            }
        }
        return words
    }

    private fun setComplexDateTypesOutOfOrdinary(words: List<Word>): List<Word> {
        var markerType: Int
        var prWord: Word?
        var curWord: Word
        var nextWord: Word?
        if (words.size > 2)
            for (i in words.indices) {
                if (i == 0 && i + 1 < words.size) {
                    prWord = null
                    curWord = words[i]
                    nextWord = words[i + 1]
                    if (isWordBelongToDay(curWord, nextWord)) {
                        words[i].typeWord = Constant.DAY
                    }
                    if (isWordBelongToYear(prWord, curWord, nextWord)) {
                        words[i].typeWord = Constant.YEAR
                    }
                } else if (i == words.size - 1) {
                    prWord = words[i - 1]
                    curWord = words[i]
                    nextWord = null
                    if (isWordBelongToDay(curWord, nextWord)) {
                        words[i].typeWord = Constant.DAY
                    }
                    if (isWordBelongToYear(prWord, curWord, nextWord)) {
                        words[i].typeWord = Constant.YEAR
                    }
                    markerType = returnIfWordIsMarker(curWord, nextWord)
                    if (markerType != -1) {
                        words[i].typeWord = markerType
                    }
                } else {
                    prWord = words[i - 1]
                    curWord = words[i]
                    nextWord = words[i + 1]
                    if (isWordBelongToDay(curWord, nextWord)) {
                        words[i].typeWord = Constant.DAY
                    }
                    if (isWordBelongToYear(prWord, curWord, nextWord)) {
                        words[i].typeWord = Constant.YEAR
                    }
                    markerType = returnIfWordIsMarker(curWord, nextWord)
                    if (markerType != -1) {
                        words[i].typeWord = markerType
                    }
                    if (isWordFutureDayMarker(prWord, curWord, nextWord))
                        words[i - 1].typeWord = Constant.FUTURE_DAY_MARKER_WITH_COUNT
                }
            }
        return words
    }

    private fun returnIfWordIsMarker(curWord: Word, nextWord: Word?): Int {
        var type = when {
            curWord.name.contains(Constant.YESTERDAY) -> Constant.PAST_DAY_MARKER
            curWord.name.contains(Constant.TOMORROW) -> Constant.FUTURE_DAY_MARKER
            else -> Constant.TYPE_NONE
        }
        if (nextWord != null) {
            if (isWordBelongToPrepositionOfFuture(curWord)) {
                type = typePrepositionFuture(nextWord)
            } else if (isWordBelongToPrepositionOfPast(curWord)) {
                type =typePrepositionPast(nextWord)
            }
        }
        return type
    }

    /**
     *
     */
    private fun typePrepositionFuture(word: Word): Int {
        var type = when (word.typeWord) {
            Constant.YEAR_WORD -> Constant.FUTURE_YEAR_MARKER
            Constant.DAY_WORD -> Constant.FUTURE_DAY_MARKER
            Constant.WEEK_WORD -> Constant.FUTURE_WEEK_MARKER
            Constant.MONTH -> Constant.FUTURE_MONTH_MARKER_WITH_NAME
            Constant.MONTH_WORD -> Constant.FUTURE_MONTH_MARKER
            else -> {
                Constant.TYPE_NONE
            }
        }
        if (isWordBelongToDayOfWeek(word)) type = Constant.FUTURE_WEEK_MARKER
        return type
    }

    /**
     *
     */
    private fun typePrepositionPast(word: Word): Int {
        return when (word.typeWord) {
            Constant.YEAR_WORD -> Constant.PAST_YEAR_MARKER
            Constant.DAY_WORD -> Constant.PAST_DAY_MARKER
            Constant.WEEK_WORD -> Constant.PAST_WEEK_MARKER
            Constant.MONTH -> Constant.PAST_MONTH_MARKER_WITH_NAME
            Constant.MONTH_WORD -> Constant.PAST_MONTH_MARKER
            else -> {
                Constant.TYPE_NONE
            }
        }
    }

    /**
     * get the day of the month out of the word
     * @w - word verifiable
     */
    private fun getMonthNumber(w: Word): Int {
        return months.firstOrNull { w.name.contains(it.word) }
                ?.type
                ?: Constant.MONTH_NONE
    }

    /**
     * get the day of the week from the word
     * @w - word verifiable
     */
    private fun getWeekDay(w: Word): Int {
        return dayOfWeeks.firstOrNull { w.name.contains(it.word) }
                ?.type
                ?: Constant.WEEK_NONE
    }

    /**
     * the word belongs to the marker of the future time
     * @prWord - previous word
     * @curWord - current word
     * @nextWord - next word
     */
    private fun isWordFutureDayMarker(prWord: Word, curWord: Word, nextWord: Word): Boolean {
        return (isWordBelongToPrepositionOfFuture(prWord)
                && isNumeric(curWord.name)
                && nextWord.typeWord == Constant.DAY_WORD)
    }

    /**
     * word belongs to data type week
     * @w - word verifiable
     */
    private fun isWordBelongToDayOfWeek(w: Word): Boolean {
        return dayOfWeeks.any { w.name.contains(it.word) }
    }

    /**
     * the word belongs to the data type month
     * @w - word verifiable
     */
    private fun isWordBelongToMonth(w: Word): Boolean {
        if (w.typeWord == Constant.MONTH) return true
        return months.any { w.name.contains(it.word) }
    }

    /**
     * the word belongs to the data type year
     * @prWord - previous word
     * @curWord - current word
     * @nextWord - next word
     */
    private fun isWordBelongToYear(prWord: Word?, curWord: Word, nextWord: Word?): Boolean {
        var isDateFound = false
        if (isNumeric(curWord.name)) {
            if (prWord != null &&
                    isPrepositionDate(prWord) &&
                    Integer.parseInt(curWord.name) > 1900) isDateFound = true
            if (nextWord != null &&
                    nextWord.typeWord == Constant.YEAR_WORD) isDateFound = true
        }
        return isDateFound
    }

    /**
     * the word belongs to the data type of the day
     * @curWord - current word
     * @nextWord - next word
     */
    private fun isWordBelongToDay(curWord: Word, nextWord: Word?): Boolean {
        if (nextWord != null) {
            if (isNumeric(curWord.name) &&
                    (isWordBelongToMonth(nextWord) ||
                            nextWord.typeWord == Constant.DAY_WORD)) {
                return true
            }
        }
        return false
    }

    /**
     *  @w - verifiable word
     *  check for affiliation with prepositions of the future
     */
    private fun isWordBelongToPrepositionOfFuture(w: Word): Boolean {
        return datePrepositions.any { it.type == Constant.DATE_PREPOSITION_FUTURE && w.name.contains(it.word) }
    }

    /**
     * @w - verifiable word
     * check for affiliation with prepositions of the past
     */
    private fun isWordBelongToPrepositionOfPast(w: Word): Boolean {
        return datePrepositions.any { it.type == Constant.DATE_PREPOSITION_PAST && w.name.contains(it.word) }
    }

    /**
     * checking text for digital ownership
     * @s - text constant
     */
    private fun isNumeric(s: String?): Boolean {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+".toRegex())
    }

    /**
     * check the preposition for the date
     * @w - verifiable word
     */
    private fun isPrepositionDate(w: Word): Boolean {
        return datePrepositions.any { it.type == Constant.DATE_PREPOSITION && w.equals(it.word) }
    }

    /**
     * Method for verifying a word with the right data type
     * @word - verifiable word
     * @t - possible word data type
     */
    private fun checkWordOnType(w: Word, t: Int): Boolean {
        return timeDateKeyWords.any { it.type == t && w.name.contentEquals(it.word) }
    }
}