package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word
import java.util.*

/**
 * The date parser is a universal class for working with a
 * date in a textual representation based on the models given
 * get the data type date
 */
interface ParserDate {

    /**
     * Typing temporary elements with markers. Using prefabricated structures
     * @words -
     */
    fun typeDateInWords(words: List<Word>): List<Word>

    /**
     * the method is designed to retrieve data using the already-archived
     * data array and the format in which the calculation will be made
     * @words -
     * @calendar -
     */
    fun getCalendarFromWords(words: List<Word>): Calendar?
}