package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word
import java.util.*

/**
 * ParserTime interface for converting text elements into a temporary element
 * using a system of prepositions
 */
interface ParserTime {
    /**
     * Typing temporary elements with markers. Using prefabricated structures
     * @words -
     */
    fun typeTimeInWords(words: List<Word>): List<Word>

    /**
     * the method is designed to retrieve data using the already-archived
     * data array and the format in which the calculation will be made
     * @words -
     * @calendar -
     */
    fun getCalendarFromWords(words: List<Word>, calendar: Calendar?): Calendar?
}