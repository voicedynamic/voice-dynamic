package com.voicedynamic.smartsearch.parser


import com.thunderrise.smartsearch.base.Word

import java.util.ArrayList

/**
 * * The sentence parser is the class basic parser for
 * breaking a sentence into a list of words, thereby removing the
 * signs of the denunciations translating words into lower case
 */
open class AppParserSentence private constructor() : ParserSentence {

    override fun getWords(stringForParsing: String): List<Word> {

        val wordList = ArrayList<Word>()

        val words = stringForParsing
                .toLowerCase()
                .split(WORD_SEPARATOR.toRegex())
                .dropLastWhile { it.isEmpty() }
                .toTypedArray().iterator()

        for (word in words) {
            wordList.add(Word(word))
        }
        return wordList
    }

    companion object {
        private var instance: AppParserSentence? = null
        private val WORD_SEPARATOR = " "

        fun newInstance(): AppParserSentence? {
            if (instance == null) {
                instance = AppParserSentence()
            }
            return instance
        }
    }
}
