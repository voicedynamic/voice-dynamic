package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word
import com.voicedynamic.smartsearch.base.Constant
import com.voicedynamic.smartsearch.model.DateTime
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class AppParserTime : ParserTime {

    private val timePrepositions: List<DateTime> = ArrayList()
    private val timeWords: List<DateTime> = ArrayList()
    private val timeDateKeyWords: List<DateTime> = ArrayList()

    override fun typeTimeInWords(words: List<Word>): List<Word> {


        return words
    }

    override fun getCalendarFromWords(words: List<Word>, cal: Calendar?): Calendar? {
        var isSomethingFound = true
        var calendar: Calendar? = cal
        if (calendar == null) {
            calendar = GregorianCalendar.getInstance()
            isSomethingFound = false
        }

        var timeType = if (words.any {
            it.typeWord == Constant.FUTURE_HOUR_MARKER ||
                    it.typeWord == Constant.FUTURE_MINUTE_MARKER ||
                    it.typeWord == Constant.FUTURE_SECOND_MARKER
        }) 1 else 0

        words.filter {
            it.typeWord == Constant.PAST_HOUR_MARKER ||
                    it.typeWord == Constant.PAST_MINUTE_MARKER ||
                    it.typeWord == Constant.PAST_SECOND_MARKER
        }.forEach { timeType = 2 }


        for (word in words) {
            when (word.typeWord) {
                Constant.TIME_FULL -> try {
                    val text = word.name
                    if (text.length == 5) {
                        val hour = text.substring(0, 2)
                        val minute = text.substring(3, 5)
                        when (timeType) {
                            0 -> {
                                calendar!!.set(Calendar.HOUR_OF_DAY, getNumFromWord(hour))
                                calendar.set(Calendar.MINUTE, getNumFromWord(minute))
                            }
                            1 -> {
                                calendar!!.add(Calendar.HOUR_OF_DAY, getNumFromWord(hour))
                                calendar.add(Calendar.MINUTE, getNumFromWord(minute))
                            }
                            2 -> {
                                calendar!!.roll(Calendar.HOUR_OF_DAY, getNumFromWord(hour))
                                calendar.roll(Calendar.MINUTE, getNumFromWord(minute))
                            }
                        }
                        isSomethingFound = true
                    } else {
                        val hour = text.substring(0, 1)
                        val minute = text.substring(2, 4)
                        when (timeType) {
                            0 -> {
                                calendar!!.set(Calendar.HOUR_OF_DAY, getNumFromWord(hour))
                                calendar.set(Calendar.MINUTE, getNumFromWord(minute))
                            }
                            1 -> {
                                calendar!!.add(Calendar.HOUR_OF_DAY, getNumFromWord(hour))
                                calendar.add(Calendar.MINUTE, getNumFromWord(minute))
                            }
                            2 -> {
                                calendar!!.roll(Calendar.HOUR_OF_DAY, getNumFromWord(hour))
                                calendar.roll(Calendar.MINUTE, getNumFromWord(minute))
                            }
                        }
                        isSomethingFound = true
                    }
                } catch (e: Exception) {

                }

                Constant.HOUR -> {
                    when (timeType) {
                        0 -> calendar!!.set(Calendar.HOUR_OF_DAY, getNumFromWord(word.name))
                        1 -> calendar!!.add(Calendar.HOUR_OF_DAY, getNumFromWord(word.name))
                        2 -> calendar!!.roll(Calendar.HOUR_OF_DAY, getNumFromWord(word.name))
                    }
                    isSomethingFound = true
                }
                Constant.MINUTE -> {
                    when (timeType) {
                        0 -> calendar!!.set(Calendar.MINUTE, getNumFromWord(word.name))
                        1 -> calendar!!.add(Calendar.MINUTE, getNumFromWord(word.name))
                        2 -> calendar!!.roll(Calendar.MINUTE, getNumFromWord(word.name))
                    }
                    isSomethingFound = true
                }
                Constant.SECOND -> {
                    when (timeType) {
                        0 -> calendar!!.set(Calendar.SECOND, getNumFromWord(word.name))
                        1 -> calendar!!.add(Calendar.SECOND, getNumFromWord(word.name))
                        2 -> calendar!!.roll(Calendar.SECOND, getNumFromWord(word.name))
                    }
                    isSomethingFound = true
                }
            }
        }

        for (word in words) {
            when (word.typeWord) {
                Constant.EVENING_MARKER -> if (isSomethingFound) {
                    val hours = calendar!!.get(Calendar.HOUR_OF_DAY)
                    if (hours <= 12)
                        calendar.set(Calendar.HOUR_OF_DAY, hours + 12)
                } else {
                    calendar!!.set(Calendar.HOUR_OF_DAY, 20)
                    calendar!!.set(Calendar.MINUTE, 0)
                    isSomethingFound = true
                }
                Constant.MORNING_MARKER -> if (isSomethingFound) {
                    val hours = calendar!!.get(Calendar.HOUR_OF_DAY)
                    if (hours >= 12)
                        calendar.set(Calendar.HOUR_OF_DAY, hours - 12)
                } else {
                    calendar!!.set(Calendar.HOUR_OF_DAY, 8)
                    calendar.set(Calendar.MINUTE, 0)
                    isSomethingFound = true
                }
                Constant.DINNER_MARKER -> {
                    calendar!!.set(Calendar.HOUR_OF_DAY, 14)
                    calendar.set(Calendar.MINUTE, 0)
                }
            }
        }

        return if (isSomethingFound)
            calendar!!
        else null
    }

    private fun getNumFromWord(text: String): Int {
        try {
            return Integer.parseInt(text)
        } catch (e: Exception) {
            timeWords.filter { text.contains(it.word) }
                    .forEach { return it.id }
        }
        return 0
    }

    private fun returnIfWordIsPastMarker(curWord: Word, nextWord: Word?): Int {
        var type = Constant.TYPE_NONE
        if (nextWord != null) {
            if (isWordBelongToPrepositionOfPast(nextWord)) {
                type = when {
                    curWord.typeWord == Constant.HOUR_WORD -> Constant.PAST_HOUR_MARKER
                    curWord.typeWord == Constant.MINUTE_WORD -> Constant.PAST_MINUTE_MARKER
                    curWord.typeWord == Constant.SECOND_WORD -> Constant.PAST_SECOND_MARKER
                    else -> {
                        Constant.TYPE_NONE
                    }
                }
            }
        }
        return type
    }

    private fun returnIfWordIsPastMarker(pastWord: Word, curWord: Word, nextWord: Word?): Int {
        var type = Constant.TYPE_NONE
        val isNumeric = isNumeric(pastWord)
        val isNumericAsWord = isNumericAsWord(pastWord)
        if (nextWord != null) {
            if (isWordBelongToPrepositionOfPast(nextWord) && (isNumeric || isNumericAsWord)) {
                type = when {
                    curWord.typeWord == Constant.HOUR_WORD -> Constant.PAST_HOUR_MARKER
                    curWord.typeWord == Constant.MINUTE_WORD -> Constant.PAST_MINUTE_MARKER
                    curWord.typeWord == Constant.SECOND_WORD -> Constant.PAST_SECOND_MARKER
                    else -> {
                        Constant.TYPE_NONE
                    }
                }
            }
        }
        return type
    }

    private fun returnIfWordIsFutureMarker(curWord: Word, nextWord: Word?): Int {
        var type = Constant.TYPE_NONE
        if (nextWord != null) {
            if (isWordBelongToPrepositionOfFuture(curWord)) {
                type = when {
                    nextWord.typeWord == Constant.HOUR_WORD -> Constant.FUTURE_HOUR_MARKER
                    nextWord.typeWord == Constant.MINUTE_WORD -> Constant.FUTURE_MINUTE_MARKER
                    nextWord.typeWord == Constant.SECOND_WORD -> Constant.FUTURE_SECOND_MARKER
                    else -> {
                        Constant.TYPE_NONE
                    }
                }
            }
        }
        return type
    }

    private fun returnIfWordIsFutureMarker(pastWord: Word?, curWord: Word, nextWord: Word?): Int {
        var type = Constant.TYPE_NONE
        val isNumeric = isNumeric(curWord)
        val isNumericAsWord = isNumericAsWord(curWord)
        if (pastWord != null && nextWord != null) {
            if (isWordBelongToPrepositionOfFuture(pastWord) && (isNumeric || isNumericAsWord)) {
                type = when {
                    nextWord.typeWord == Constant.HOUR_WORD -> Constant.FUTURE_HOUR_MARKER
                    nextWord.typeWord == Constant.MINUTE_WORD -> Constant.FUTURE_MINUTE_MARKER
                    nextWord.typeWord == Constant.SECOND_WORD -> Constant.FUTURE_SECOND_MARKER
                    else -> {
                        Constant.TYPE_NONE
                    }
                }
            }
        }
        return type
    }

    private fun checkWordOnFullTimePatterns(word: Word): Boolean {
        val patterns = java.util.ArrayList<Pattern>()
        patterns.add(Pattern.compile("^(([0,23])|(2[0-3])):[0-5][0-9]$"))
        patterns.add(Pattern.compile("^(([0,1][0-9])|(2[0-3])):[0-5][0-9]$"))
        patterns.add(Pattern.compile("^([0-9]):[0-5][0-9]$"))
        return patterns.any { it.matcher(word.name).matches() }
    }

    private fun isWordBelongToHour(curWord: Word, nextWord: Word?): Boolean {
        if (nextWord != null) {
            if ((isNumeric(curWord) ||
                    isNumericAsWord(curWord)) &&
                    nextWord.typeWord == Constant.HOUR_WORD) {
                return true
            }
        }
        return false
    }

    private fun isWordBelongToMinutes(prevWord: Word?, curWord: Word, nextWord: Word?): Boolean {
        if (nextWord != null) {
            if ((isNumeric(curWord) ||
                    isNumericAsWord(curWord)) &&
                    nextWord.typeWord == Constant.MINUTE_WORD) {
                if (prevWord != null && isPrepositionTime(prevWord)) {
                    prevWord.typeWord = Constant.PREPOSITION_DATE
                }
                return true
            }
        }
        return false
    }

    private fun isWordBelongToSeconds(curWord: Word, nextWord: Word?): Boolean {
        if (nextWord != null) {
            if ((isNumeric(curWord) || isNumericAsWord(curWord))
                    && nextWord.typeWord == Constant.SECOND_WORD) {
                return true
            }
        }
        return false
    }

    private fun checkWordOnType(word: Word, type: Int): Boolean {
        return timeDateKeyWords.any { it.type == type && word.name.contentEquals(it.word) }
    }

    private fun isNumeric(word: Word): Boolean {
        return word.name.matches("[-+]?\\d*\\.?\\d+".toRegex())
    }

    private fun isNumericAsWord(word: Word): Boolean {
        return timeWords.any { word.name.contentEquals(it.word) }
    }

    private fun isPrepositionTime(word: Word): Boolean {
        return if (timePrepositions.any { it.word.contentEquals(word.name) }) true else true
    }

    private fun isWordBelongToPrepositionOfFuture(word: Word): Boolean {
        return timePrepositions.any { it.type == Constant.TIME_PREPOSITION_FUTURE && word.name.contains(it.word) }
    }

    private fun isWordBelongToPrepositionOfPast(word: Word): Boolean {
        return timePrepositions.any { it.type == Constant.TIME_PREPOSITION_PAST && word.name.contains(it.word) }
    }
}