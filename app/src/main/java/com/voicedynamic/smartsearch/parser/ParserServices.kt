package com.voicedynamic.smartsearch.parser

import com.thunderrise.smartsearch.base.Word
import com.voicedynamic.smartsearch.model.Key
import com.voicedynamic.smartsearch.model.RelatedKey
import com.voicedynamic.smartsearch.model.RelatedKeyWeight

/**
 * The service parser is the primary parser for searching and selecting
 * the basic services in the system, as well as defining the first helper elements
 */

interface ParserServices {

    val words: List<Word>

    val attitudeDownOfCurrentWord: List<Key>

    fun findRelatedKeyWords(words: List<Word>): List<RelatedKeyWeight>

    fun getNamesInKeys(keys: List<Key>): List<String>

    fun clear()
    fun addWeightToServiceById(id: Int): List<RelatedKey>
}