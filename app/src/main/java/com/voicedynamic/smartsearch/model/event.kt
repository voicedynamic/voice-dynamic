package com.voicedynamic.smartsearch.model

import com.google.firebase.database.PropertyName


/**
 *
 */
class Attribute(
    @PropertyName("key") val key: String,
    @PropertyName("name_ru") val nameRu: String,
    @PropertyName("question") val question: String,
    @PropertyName("status") val status: Boolean,
    @PropertyName("type") val type: Int,
    @PropertyName("value") val value: String,
    @PropertyName("required") val required: Boolean,
    @PropertyName("default") val default: Default
)

/**
 *
 */
class Default(
    @PropertyName("value") val value: String,
    @PropertyName("question") val question: String
)

/**
 *
 */
class Event(
    @PropertyName("id") val id: Int,
    @PropertyName("name") val name: String,
    @PropertyName("type") val type: String,
    @PropertyName("success") val successMessage: String,
    @PropertyName("cancel") val cancelMessage: String,
    @PropertyName("rescission_words") val rescissionWords: List<String>,
    @PropertyName("keywords") val keywordsId: List<Int>,
    @PropertyName("attributes") val attributes: List<Attribute>,
    @PropertyName("action") val action: String
){
    override fun equals(event: Any?): Boolean {
        if (event is Event) {
            if (id==event.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$name"
    }
}

/**
 *
 */
class KeyQuestions(
    @PropertyName("key") var key: String,
    @PropertyName("questions") var questions: List<String>
) {
    override fun equals(keyQuestions: Any?): Boolean {
        if (keyQuestions is QuickQuestions) {
            if (key.equals(keyQuestions.key)) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$key"
    }
}

/**
 *
 */
class QuickQuestions(
    @PropertyName("question") val key: String,
    @PropertyName("answers") val questions: List<String>
) {
    override fun equals(quickQuestions: Any?): Boolean {
        if (quickQuestions is QuickQuestions) {
            if (key.equals(quickQuestions.key)) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$questions"
    }
}