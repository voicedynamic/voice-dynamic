package com.voicedynamic.smartsearch.model

import com.google.firebase.database.PropertyName

class Action(
    @PropertyName("id") val id: Int,
    @PropertyName("root") val root: String,
    @PropertyName("words") val words: List<String>,
    @PropertyName("anEcommerceAction") val isAnEcommerceAction: Boolean
) {
    override fun equals(action: Any?): Boolean {
        if (action is Action) {
            if (id == action.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $root"
    }
}

class CompatibleAction(
    @PropertyName("rootAction") val isFirstAction: Boolean,
    @PropertyName("firstAction") val firstAction: Int,
    @PropertyName("secondAction") val secondAction: Int
) {
    override fun equals(compatibleAction: Any?): Boolean {
        if (compatibleAction is CompatibleAction) {
            if (firstAction.equals(compatibleAction.firstAction) &&
                secondAction.equals(compatibleAction.secondAction)
            ) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$firstAction = $secondAction"
    }
}

class Key(
    @PropertyName("id") var id: Int,
    @PropertyName("root_word") var rootWord: String,
    @PropertyName("words") var words: List<String>,
    @PropertyName("main_actions") var mainActions: List<Int>,
    @PropertyName("support_actions") var supportActions: List<Int>,
    @PropertyName("number_of_uses") var numberOfUses: Int,
    @PropertyName("is_company") var isCompany: Boolean
) {
    override fun equals(key: Any?): Boolean {
        if (key is Key) {
            if (id == key.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $rootWord"
    }
}

class RelatedKey(
    @PropertyName("id") var id: Int,
    @PropertyName("level") var level: Int,
    @PropertyName("id_key") var keysId: List<Int>,
    @PropertyName("id_dictionary") var dictionaryId: List<Int>,
    @PropertyName("attitude_down") var attitudeDownId: List<Int>,
    @PropertyName("weight") var weight: Int
) {
    override fun equals(relatedKey: Any?): Boolean {
        if (relatedKey is RelatedKey) {
            if (id == relatedKey.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $weight"
    }
}