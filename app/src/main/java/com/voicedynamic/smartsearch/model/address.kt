package com.voicedynamic.smartsearch.model

import com.google.firebase.database.PropertyName
import com.thunderrise.smartsearch.base.Word

/**
 * Class address is a list of words that refer to the addresses in them does not include the house number.
 * The house number goes like an attribute. Also, the address can be either completed or for example on
 * a trc terra. Terra is attached to an address. And the streets on which it stands can be several.
 * @id - identification;
 * @root - root of the word;
 * names - list of word options;
 * @completeAddress - this is the final address;
 * @numberOfUses - number of uses in other phrases;
 */

class Address(
    @PropertyName("id") val id: Int,
    @PropertyName("root") val root: List<String>,
    @PropertyName("names") val names: List<String>,
    @PropertyName("completeAddress") val isCompleteAddress: Boolean,
    @PropertyName("numberOfUses") val numberOfUses: Int
) {

    override fun equals(address: Any?): Boolean {
        if (address is Address) {
            if (root == address.root) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        var str = ""
        for (name in names) {
            str += "$name "
        }
        return str
    }
}

/**
 * AddressQuestion class that carries all the auxiliary
 * questions to clarify the address
 * @id - identification
 * @questions - clarifying question
 */
class AddressQuestion(
    @PropertyName("id") val id: Int,
    @PropertyName("question") val question: String
) {
    override fun equals(addressQuestion: Any?): Boolean {
        if (addressQuestion is AddressQuestion) {
            if (id == addressQuestion.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $question"
    }
}

/**
 * AddressRelatedVariation The base class that is used to find the options for the
 * address that stores the data about the object's mass.
 * @address - the word that is used in the address
 * @currentRelatedVariation - The current key that shows at what
 * stage the search stopped
 * @idRelatedKeys - list of keys to move down the word relationship tree
 * @weight - current word weight
 */
class AddressRelatedVariation(
    val address: String,
    val currentRelatedKey: Int,
    val idRelatedKeys: List<Int>,
    val weight: Int
) {
    override fun equals(addressRelatedVeight: Any?): Boolean {
        if (addressRelatedVeight is AddressRelatedVariation) {
            if (address.equals(addressRelatedVeight.address)) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$address $weight"
    }
}

/**
 * Keeps a list of words with their movement also contains
 * a keyword index by which the search goes on.
 * @id - identification
 * @roots - root addresses list
 * @names - word list addresses
 * @words - current words
 * @weight - the weight of this variant of the address
 */
class AddressVariation(
    val id: Int,
    val roots: List<String>,
    val names: List<String>,
    val words: List<Word>,
    var weight: Int
) {
    override fun equals(addressVariation: Any?): Boolean {
        if (addressVariation is AddressVariation) {
            if (id == addressVariation.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        var str = "$id "
        for (word in words) {
            str += "$word "
        }
        return str
    }
}

/**
 * Related Key Address class for linking words to
 * speech constructs based on word address identifiers
 * @id - identification
 * @idAddress - a list of words that are similar in meaning
 * @idAddressQuestion -  question identifier for clarifying the address;
 * @attitudeDowns - address identifiers that may be adjacent to the address
 * @weight - weight between adjacent words
 */
class RelatedKeyAddress(
    val id: Int,
    val idAddress: List<Int>,
    val idAddressQuestion: Int,
    val attitudeDowns: List<Int>?,
    val weight: Int
) {
    override fun equals(relatedKeyAddress: Any?): Boolean {
        if (relatedKeyAddress is RelatedKeyAddress) {
            if (id == relatedKeyAddress.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $idAddress $weight"
    }
}

/**
 * class that tells about the street. Number of houses on this street.
 * type of street (street, avenue, lane). The longitude and latitude
 * of this street are also shown
 * @id - identification
 * @typeOfStreet - type of street (alley, street, avenue)
 * @numberHouse - list of houses on the current street
 * @latitude - the latitude on which the street stands
 * @langitude - the langitude on which the street stands
 */
class Street(
    @PropertyName("id") val id: Int,
    @PropertyName("typeOfStreet") val typeOfStreet: Int,
    @PropertyName("numberHouse") val numberHouse: List<Int>,
    @PropertyName("latitude") val latitude: Int,
    @PropertyName("langitude") val langitude: Int
) {
    override fun equals(street: Any?): Boolean {
        if (street is Street) {
            if (id == street.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $latitude $langitude"
    }
}

/**
 * Class for processing street types such as (prospectuses)
 * @id - identification
 * @root - street identifier root
 * @names - street list identifier names
 */
class StreetType(
    @PropertyName("id") val id: Int,
    @PropertyName("root") val root: String,
    @PropertyName("names") val names: List<String>
) {
    override fun equals(streetType: Any?): Boolean {
        if (streetType is StreetType) {
            if (id == streetType.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        var str = "$id "
        for (name in names) {
            str += "$name "
        }
        return str
    }
}

