package com.voicedynamic.smartsearch.model

import com.google.firebase.database.PropertyName

/**
 * A data class that allows the user to use additional
 * dictionaries to increase search ability
 * @id -
 * @root -
 * @name -
 * @keyNames -
 * @type -
 */
class DictionaryWord(
    @PropertyName("id") var id: Int,
    @PropertyName("root") val root: String,
    @PropertyName("name") var name: List<String>,
    @PropertyName("key_names") var keyNames: List<Int>,
    @PropertyName("type") var type: Int = 0
) {
    override fun equals(dictionaryWord: Any?): Boolean {
        if (dictionaryWord is DictionaryWord) {
            if (id == dictionaryWord.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $name"
    }
}