package com.voicedynamic.smartsearch.model

class RelatedKeyWeight(
    val companyName: String,
    val idrelatedKey: Int,
    val weight: Int,
    val wBetweenServices: Int,
    val wAction: Int,
    val wComppany: Int,
    val wDictionary: Int
) : Cloneable {
    override fun equals(relatedKeyWeight: Any?): Boolean {
        if (relatedKeyWeight is RelatedKeyWeight) {
            if (companyName.equals(relatedKeyWeight.companyName)) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$companyName $weight"
    }
}

class Sentence(
    val id: Int = -1,
    val sentence: String = ""
)