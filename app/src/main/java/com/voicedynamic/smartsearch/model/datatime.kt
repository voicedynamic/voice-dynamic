package com.voicedynamic.smartsearch.model

import com.google.firebase.database.PropertyName

/**
 * The time-out class is the base class for working with the date and time.
 * This class is a data class that accepts input data for words and their type.
 * @id -
 * @type -
 * @word -
 */
class DateTime(
    @PropertyName("id") val id: Int,
    @PropertyName("type") val type: Int,
    @PropertyName("word") val word: String
) {
    override fun equals(dateTime: Any?): Boolean {
        if (dateTime is DateTime) {
            if (id == dateTime.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $type $word"
    }
}