package com.voicedynamic.smartsearch.model

import com.google.firebase.database.PropertyName

/**
 * The class of associations includes data that are
 * associative for many people, ie those that carry
 * meaning only for a particular user or group of users.
 * Also, the data denote other semantic forms
 * @id -
 * @rootWord -
 * @word -
 * @mainAction -
 * @supportAction -
 * @numberOfUses -
 * @company -
 */
class Association(
    @PropertyName("id") val id: Int,
    @PropertyName("rootWord") val rootWord: String,
    @PropertyName("word") val word: List<String>,
    @PropertyName("main_actions") val mainActions: List<Int>,
    @PropertyName("supportActions") val supportActions: List<Int>,
    @PropertyName("numberOfUses") val numberOfUses: Int,
    @PropertyName("company") val isCompany: Boolean
) {
    override fun equals(association: Any?): Boolean {
        if (association is Association) {
            if (id == association.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $rootWord"
    }
}

/**
 * RelatedAssociation class is a class that associates words to create a dependency tree
 * @id -
 * @keys -
 * @attitudeDown -
 * @isFinal -
 * @type -
 * @weight -
 */
class RelatedAssociation(
    @PropertyName("id") val id: Int,
    @PropertyName("keys") val keysId: List<Int>,
    @PropertyName("attitudeDown") val attitudeDownIds: List<Int>,
    @PropertyName("is_final") val isFinal: Boolean,
    @PropertyName("type") val type: Int,
    @PropertyName("weight") val weight: Int
) {
    override fun equals(relatedAssociation: Any?): Boolean {
        if (relatedAssociation is RelatedAssociation) {
            if (id == relatedAssociation.id) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$id $isFinal $weight"
    }
}

/**
 * RelatedAssociationWeight base class for finding possible options
 * for the given mass of proposal data
 * @companyName -
 * @idRelatedKey -
 * @isFinalValue -
 * @weight -
 */
class RelatedAssociationWeight(
    val companyName: String,
    val idRelatedKey: Int,
    val isFinalValue: Boolean,
    val weight: Int
) {
    override fun equals(relatedAssociation: Any?): Boolean {
        if (relatedAssociation is RelatedAssociationWeight) {
            if (idRelatedKey == relatedAssociation.idRelatedKey) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$companyName $weight"
    }
}
